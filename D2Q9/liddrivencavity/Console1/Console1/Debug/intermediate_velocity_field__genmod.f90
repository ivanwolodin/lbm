        !COMPILER-GENERATED INTERFACE MODULE: Mon Apr 01 10:33:19 2019
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE INTERMEDIATE_VELOCITY_FIELD__genmod
          INTERFACE 
            SUBROUTINE INTERMEDIATE_VELOCITY_FIELD(U,V,RHO,UO,N,M,KK)
              INTEGER(KIND=4) :: M
              INTEGER(KIND=4) :: N
              REAL(KIND=4) :: U(0:N,0:M)
              REAL(KIND=4) :: V(0:N,0:M)
              REAL(KIND=4) :: RHO(0:N,0:M)
              REAL(KIND=4) :: UO
              INTEGER(KIND=4) :: KK
            END SUBROUTINE INTERMEDIATE_VELOCITY_FIELD
          END INTERFACE 
        END MODULE INTERMEDIATE_VELOCITY_FIELD__genmod
