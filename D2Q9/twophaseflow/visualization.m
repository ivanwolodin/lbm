
%clc;clear;

%% ������� ����� 
Nx = 11;%301;
Ny = 8;%41;
Grid_Size = Nx*Ny;

%% name of file
file_name = 'rho_evolution.dat';
%file_name = 'evolutionVx1.dat';
 %file_name = 'evolutionVy1.dat';
%file_name = 'rho.dat';
% file_name = 'u.dat';
%file_name = 'v.dat';


%% ��������� �� 
%{%}
fileI = fopen(file_name,'r');
out = fscanf(fileI,'%f');
fclose(fileI);
VWhole = reshape(out,Grid_Size, []);
  
%% Gif  /�������� �������/
%{
[m,n] = size(VWhole);
number_of_steps = n;
for step_number=1:number_of_steps
    
    out = VWhole(: , step_number);
    V = reshape(out,Ny, []);
    figure(2)
    imagesc(V );
    xlabel('x');
    ylabel('y');
    title('Velocity field visualized by color gradient');
    colorbar;
    set(gca,'YDir','normal');
    drawnow
    step_number % ���������� ���
    pause(0.03)

end
%}
%{
%% vector velocity field //
%{%}
file1 = fopen('Vx1','r');
data_from_file_Vx = fscanf(file1,'%f');
fclose(file1);
%Vx = reshape(out,101, []);

file2 = fopen('Vy1','r');
data_from_file_Vy = fscanf(file2,'%f');
fclose(file2);

%Vy = reshape(out,101, []);

x = 0:(Nx-1);
y = 0:(Ny-1);
[X,Y] = meshgrid(x,y);

Vx_whole = reshape(data_from_file_Vx,Grid_Size, []);
Vy_whole = reshape(data_from_file_Vy,Grid_Size, []);
[m,n] = size(Vx_whole);
number_of_steps = n;

for step_number=1:number_of_steps
    
    Vx = Vx_whole(: , step_number);
    Vx = reshape(Vx,Ny, []);
    
    Vy = Vy_whole(: , step_number);
    Vy = reshape(Vy,Ny, []);
    
    figure(3)
    quiver(X(1:5:end),Y(1:5:end),Vx(1:5:end),Vy(1:5:end))
    %quiver(X,Y,Vx,Vy)
    %startx = 0.1:0.1:1;
    %starty = ones(size(startx));
    %streamline(X(1:2:end),Y(1:2:end),Vx(1:2:end),Vy(1:2:end),startx,starty)
    xlabel('x');
    ylabel('y');
    title('Vector velocity field');
    
    axis([0 Nx 0 Ny])
    
    drawnow
    step_number % ���������� ���
    pause(0.05)

end
%}

%% /Single shot/
%{
step_number = 120
    Vx = Vx_whole(: , step_number);
    Vx = reshape(Vx,Ny, []);
    
    Vy = Vy_whole(: , step_number);
    Vy = reshape(Vy,Ny, []);
    
    figure(2)
    discret = 5;
    quiver(X(1:discret:end),Y(1:discret:end),Vx(1:discret:end),Vy(1:discret:end))

    xlabel('x');
    ylabel('y');
    title('Vector velocity field');
    
    axis([0 Nx 0 Ny])

%}
%% �������� ���  /Single shot/
%{%}

step_time = 2;
out = VWhole(: , step_time);
V = reshape(out,Ny, []);

figure(step_time)
imagesc(V);
%imagesc(V);
xlabel('x');
ylabel('y');
%ylim([1,5]);
%xlim([1,11]);
title('Velocity field visualized by color gradient');

colorbar;
set(gca,'YDir','normal')
