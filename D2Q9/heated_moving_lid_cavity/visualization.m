clc;clear;
fileI = fopen('tprof','r');
out = fscanf(fileI,'%f');
fclose(fileI);
T = reshape(out,201, []);

figure(1)
imagesc(T);

colorbar;
set(gca,'YDir','normal')
xlabel('x');
ylabel('y');
title('Temperature field');
%{%}
file1 = fopen('uvely.txt','r');
out = fscanf(file1,'%f');
fclose(file1);
Vx = reshape(out,201, []);

file2 = fopen('vvelx.txt','r');
out = fscanf(file2,'%f');
fclose(file2);
Vy = reshape(out,201, []);

x = 0:200;
y = 0:200;
[X,Y] = meshgrid(x,y);
Vx = Vx.*100;
Vy = Vy.*100;
figure(2)
quiver(X(1:7:end),Y(1:7:end),Vx(1:7:end),Vy(1:7:end))
xlabel('x');
ylabel('y');
title('Velocity field');


